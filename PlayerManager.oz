functor
import
   Player003BasicAI
   Player003AdvancedAI
   PlayerBasicAI
export
   playerGenerator:PlayerGenerator
define
   PlayerGenerator
in
   fun{PlayerGenerator Kind Color ID}
      case Kind
      of player003basicai then {Player003BasicAI.portPlayer Color ID}
      [] player003advancedai then {Player003AdvancedAI.portPlayer Color ID}
      [] basicAI then {PlayerBasicAI.portPlayer Color ID}

      end
   end
end
