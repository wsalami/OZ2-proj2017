functor
import
   Input
   Browser
   OS
export
   portPlayer:StartPlayer
define
   StartPlayer
   FindInitialPosition
   Direction
   TreatStream
   FreeSpace
   ReturnColumn
   CheckDamageNumber
   PreviousPos
in
   fun{StartPlayer Color ID}
      Stream
      Port
   in
      {NewPort Stream Port}
      thread
         %{TreatStream Stream MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
         {TreatStream Stream id(id:ID color:Color name:'Roger') nil 4 false 0 nil nil 0}
      end
      Port
   end

   fun{FindInitialPosition Map}
      FindInitialPositionRec in
      fun{FindInitialPositionRec Map I J}
         case Map
         of H|T then
            CheckInRow Pos in
            fun{CheckInRow Row I J}
               case Row
               of Ha|Ta then
                  if Ha == 0 then
                     pt(x:I y:J)
                  else {CheckInRow Ta I J+1}
                  end
               []nil then unit
               end
            end
            Pos = {CheckInRow H I J}
            if I == Input.nRow then
               Pos
            elseif Pos == unit then
               {FindInitialPositionRec T I+1 1}
            else
               Pos
            end
         end
         end
      {FindInitialPositionRec Map 1 1}
   end

  %Retourne la valeur de la case X Y
   fun{FreeSpace PosList X Y}
      fun {IsOOB X Y}
         if X > Input.nRow orelse Y > Input.nColumn orelse X < 1 orelse Y < 1 then true
         else false
         end
      end
      in
      if {IsOOB X Y} then false
      else
         case PosList of H|T then
            if X == 1 then {ReturnColumn H Y}
            else {FreeSpace T X-1 Y}
            end
         []nil then false
         end
      end
   end

   %Cherche la case dans la ligne Actuelle
   fun{ReturnColumn Row Y}
      case Row of nil then false
      []H|T then
         if Y == 1 then
            if H == 0 then true
            else false
            end
         else {ReturnColumn T Y-1}
         end
      end
   end

   fun{PreviousPos Position PosList}
     case PosList of nil then true
     []H|T then
        if H == Position then false
        else {PreviousPos Position T}
        end
     end
   end

   proc{CheckDamageNumber Expos PlayerPos Damage}
      Distance in
      Distance = {Number.abs Expos.x - PlayerPos.x} + {Number.abs Expos.y - PlayerPos.y}
      case Distance of 1 then Damage = 1
      [] 0 then Damage = 2
      else Damage = 0
      end
   end

   proc{TreatStream Stream MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft} % has as many parameters as you want
      case Stream
      of nil then skip
      [] initPosition(?ID ?Position)|T then
         Tamp in
         Tamp = {FindInitialPosition Input.map}
         if Tamp == unit then
            Position = no
         else
            Position = Tamp
         end
         ID = MyID
         {TreatStream T MyID Tamp LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
      [] dive|T then
         {TreatStream T MyID MyPos LifeLeft false MineNumber Mines nil SurfacedCountLeft}
      [] isSurface(?ID ?Answer)|T then
         if LifeLeft =< 0 then
           ID = nil
           Answer = Surfaced
         else
           ID = MyID
           Answer = Surfaced
         end
         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft-1}
      [] move(?ID ?Position ?Direction)|T then
         Rand NewPos NewDir in
         ID = MyID
         Rand = ({OS.rand} mod 5) + 1
         case Rand of 1 then
            NewPos = pt(x:MyPos.x-1 y:MyPos.y)
            NewDir = north
         [] 2 then
            NewPos = pt(x:MyPos.x+1 y:MyPos.y)
            NewDir = south
         [] 3 then
            NewPos = pt(x:MyPos.x y:MyPos.y-1)
            NewDir = west
         [] 4 then
            NewPos = pt(x:MyPos.x y:MyPos.y+1)
            NewDir = east
         [] 5 then
            NewPos = MyPos
            NewDir = surface
         end
         if NewDir == surface then
            Position = MyPos
            Direction = surface
            {TreatStream T MyID NewPos LifeLeft true MineNumber Mines PosList Input.turnSurface}
         elseif {FreeSpace Input.map NewPos.x NewPos.y} == true then
            if {PreviousPos NewPos PosList} then
              Position = NewPos
              Direction = NewDir
              {TreatStream T MyID NewPos LifeLeft Surfaced MineNumber Mines NewPos|PosList SurfacedCountLeft}
            else {TreatStream Stream MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
            end
         else
            {TreatStream Stream MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
         end
      [] chargeItem(?ID ?KindItem)|T then
         KindItem = mine
         ID = MyID
         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber+1 Mines PosList SurfacedCountLeft}
      [] fireItem(?ID ?KindFire)|T then
         X Y in
         ID = MyID
         if MineNumber > 0 then
            X = {OS.rand} mod Input.nRow + 1
            Y = {OS.rand} mod Input.nColumn + 1
            if {FreeSpace Input.map X Y} == false then KindFire = nil {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
            else
            KindFire = mine(pt(x:X y:Y))
            {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber-1 mine(pt(x:X y:Y))|Mines PosList SurfacedCountLeft}
            end
         else
            KindFire = nil
            {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
         end
      [] fireMine(?ID ?Mine)|T then
         ID = MyID
         case Mines of nil then
            Mine = nil
            {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
         []H2|T2 then
            Mine = H2
            {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber T2 PosList SurfacedCountLeft}
         end
      [] sayMove(ID Direction)|T then
         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
      [] saySurface(ID)|T then

         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
      [] sayCharge(ID KindItem)|T then

         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
      [] sayMinePlaced(ID)|T then

         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
      [] sayMissileExplode(ID Position ?Message)|T then
          Damage NextLife in
          {CheckDamageNumber Position MyPos Damage}
          NextLife = LifeLeft - Damage
          if ID == MyID orelse LifeLeft < 1 then Message = nil
          elseif NextLife < 1 then Message = sayDeath(MyID)
          elseif NextLife < LifeLeft then Message = sayDamageTaken(MyID Damage NextLife)
          else Message = nil
          end
         {TreatStream T MyID MyPos NextLife Surfaced MineNumber Mines PosList SurfacedCountLeft}
      [] sayMineExplode(ID Position ?Message)|T then
          Damage NextLife in
          {CheckDamageNumber Position MyPos Damage}
          NextLife = LifeLeft - Damage
          if ID == MyID orelse LifeLeft < 1 then Message = nil
          elseif NextLife < 1 then Message = sayDeath(MyID)
          elseif NextLife < LifeLeft then Message = sayDamageTaken(MyID Damage NextLife)
          else Message = nil
          end
         {TreatStream T MyID MyPos NextLife Surfaced MineNumber Mines PosList SurfacedCountLeft}
      [] sayPassingDrone(Drone ?ID ?Answer)|T then
          ID = MyID
          case Drone of drone(row X) then if MyPos.x == X then Answer = true else Answer = false end
          []drone(column Y) then if MyPos.y == Y then Answer = true else Answer = false end
          else Answer = false
          end
         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
      [] sayAnswerDrone(Drone ID Answer)|T then

         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
      [] sayPassingSonar(?ID ?Answer)|T then
          Rand in
          Rand = {OS.rand} mod Input.nColumn + 1
          ID = MyID
          Answer = pt(x:MyPos.x y:Rand)
         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
      [] sayAnswerSonar(ID Answer)|T then

         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
      [] sayDeath(ID)|T then

         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
      [] sayDamageTaken(ID Damage LifeLeft)|T then

         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
      [] _|T then
         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SurfacedCountLeft}
      end
   end
end
