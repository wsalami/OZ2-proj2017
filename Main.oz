functor
import
   GUI
   Input
   PlayerManager
   Browser
   OS
define
   GUIPort = {GUI.portWindow}
   InitPlayers
   InitPositions
   ListOfPortsOfPlayers
   TurnByTurn
   Simultaneous
   WideBroadCast
   MovePlayer
   IsAtSurface
   ChargePlayer
   FirePlayer
   Explosed
   DivePlayer
   MinePlayer
   RandomThink
in
   %Launch the interface for the GUI
   {Port.send GUIPort buildWindow}

   %Players is the list of players, Colors is the list of colors corresponding to the players in players,
   %I is the ID assigned to the player.
   %Returns in ListP the list of the port objects of the different players in the game.
   fun {InitPlayers Players Colors I L}
      case Players|Colors
      of (H1|T1)|(H2|T2) then
         {InitPlayers T1 T2 I+1 {PlayerManager.playerGenerator H1 H2 I}|L}
      []nil|nil then L
      end
   end

   %Send messages to the ports of the players to init their positions on the map.
   %List is the List of the ports of the players.
   proc {InitPositions List}
      case List of nil then skip
      [] H|T then
         ID Position in
	       {Port.send H initPosition(ID Position)}
	       {Send GUIPort initPlayer(ID Position)}
         {InitPositions T}
      end
   end

   %Broadcast Message to everybody
   proc {WideBroadCast Message Players}
      case Players of nil then skip
      [] H|T then
         {Port.send H Message}
         {WideBroadCast Message T}
      end
   end

   %Fonction pour bouger le joueur associé au stream
   fun{MovePlayer PlayerStream}
      ID Position Direction
      in
      {Port.send PlayerStream move(ID Position Direction)}
      {Port.send GUIPort movePlayer(ID Position)}
      if Direction == surface then
         {WideBroadCast saySurface(ID) ListOfPortsOfPlayers}
         false
      else
         {WideBroadCast sayMove(ID Direction) ListOfPortsOfPlayers}
         true
      end
   end
   %Function to know if the submarine is at surface
   fun{IsAtSurface PlayerStream}
      ID Answer in
      {Port.send PlayerStream isSurface(ID Answer)}
      if Answer then {Port.send GUIPort surface(ID)}
      end
      Answer
   end
   %Fonction pour diver le joueur associé au stream
   proc{DivePlayer PlayerStream}
      {Port.send PlayerStream dive}
   end
   %Fonction pour charger un item
   fun{ChargePlayer PlayerStream}
      KindItem ID in
      {Port.send PlayerStream chargeItem(ID KindItem)}
      {WideBroadCast sayCharge(ID KindItem)  ListOfPortsOfPlayers}
      KindItem
   end

   %Fonction pour tirer un item
   fun{FirePlayer PlayerStream PlayersLeft}
      KindFire ID in
      %{Browser.browse 'Fired'}
      {Port.send PlayerStream fireItem(ID KindFire)}
      case KindFire of mine(X) then
           %{Browser.browse 'Mined'}
           {Port.send GUIPort putMine(ID X)}
           {WideBroadCast sayMinePlaced(ID) ListOfPortsOfPlayers}
           PlayersLeft
      []missile(X) then
          %{Browser.browse 'Missiled'}
          {Port.send GUIPort explosion(ID X)}
          {Explosed missile ID X ListOfPortsOfPlayers PlayersLeft}
      else PlayersLeft
      end
   end

   %Explosion Handler
   fun{Explosed Type ID POS ListPlayers PlayersLeft}
       %{Browser.browse 'Explosed'}
      case ListPlayers of nil then PlayersLeft
      []H|T then
         Message in
         case Type of mine then
             {Port.send H sayMineExplode(ID POS Message)}
         else
             {Browser.browse 'BeforeMsg'}
             {Port.send H sayMissileExplode(ID POS Message)}
             {Browser.browse 'AfterMessage'|Message}
         end
         {WideBroadCast Message ListOfPortsOfPlayers}
         case Message of nil then {Explosed Type ID POS T PlayersLeft}
         [] sayDamageTaken(IdDamaged Damage LifeLeft) then
            {Browser.browse 'Damage said'}
            {Port.send GUIPort lifeUpdate(IdDamaged LifeLeft)}
            {Explosed Type ID POS T PlayersLeft}
         [] sayDeath(IDeath) then
            {Browser.browse 'Death said'}
            {Port.send GUIPort removePlayer(IDeath)}
            {Explosed Type ID POS T (PlayersLeft-1)}
         else {Explosed Type ID POS T PlayersLeft}
         end
      end
   end

   %Fonction pour tirer mine
   fun{MinePlayer PlayerStream PlayersLeft}
      ID Mine in
      %{Browser.browse 'Mining'}
      {Port.send PlayerStream fireMine(ID Mine)}
      {Browser.browse Mine}
      case Mine of mine(X) then
        Message Left in
        %{Browser.browse 'Mining at'|X}
        {Port.send GUIPort explosion(X)}
        {Port.send GUIPort removeMine(ID X)}
        %{Browser.browse 'Before Explosed'}
        {Explosed mine ID X ListOfPortsOfPlayers PlayersLeft}
      else {Browser.browse 'NotMining'} PlayersLeft
      end
   end

   fun{RandomThink Min Max}
      ({OS.rand} mod (Max - Min))+Min
   end

   %===========================================================================
   %===========================================================================

   %ListPlayers is the list of ports of the players
   %Count is the count of the number of turns produced to date
   proc {TurnByTurn ListPlayers}

      %Déroulement d'un tour
      proc {NewTurn ListPlayers PlayersLeft}
        {Browser.browse 'ok'}
        case ListPlayers of nil then
           if PlayersLeft > 1 then
             {NewTurn ListOfPortsOfPlayers PlayersLeft}
           end
        [] H|T then
          ID Answer Left in
          {Port.send H isSurface(ID Answer)}
          {Browser.browse Answer}
          if ID == nil then {Browser.browse 'okNil'} {NewTurn T PlayersLeft}
          elseif Answer == true then
              {Browser.browse '===================================='}
              Left = PlayersLeft
              {Port.send GUIPort surface(ID)}
              {DivePlayer H}  %On dive si on était surfaced
          else %Pas de thread en tour par tour
              {Browser.browse 'ok5'}
              if {MovePlayer H} == false then {Browser.browse 'ok6'} {NewTurn T PlayersLeft}
              else Charged Fired in
                Charged = {ChargePlayer H}
                Fired = {FirePlayer H PlayersLeft}
                Left = {MinePlayer H  Fired}
              end
          end
          {NewTurn T Left}
        end
      end
      in
      {NewTurn ListOfPortsOfPlayers Input.nbPlayer}
   end

   proc {Simultaneous ListPlayers}
      proc {SubMarineTurn Stream Count}
          if Count > 0 then
              {Delay {RandomThink Input.thinkMin Input.thinkMax}}
              {SubMarineTurn Stream Count-1}
          else
              ID Answer in
              {Port.send Stream isSurface(ID Answer)}
              if ID == nil then skip
              else
                  {DivePlayer Stream}
                  {Delay {RandomThink Input.thinkMin Input.thinkMax}}
                  if {MovePlayer Stream} == false then
                      {Port.send GUIPort surface(ID)}
                      {Delay {RandomThink Input.thinkMin Input.thinkMax}}
                      {SubMarineTurn Stream Input.turnSurface}
                  else Charged Fired Left in
                      {Delay {RandomThink Input.thinkMin Input.thinkMax}}
                      Charged = {ChargePlayer Stream}
                      {Delay {RandomThink Input.thinkMin Input.thinkMax}}
                      Fired = {FirePlayer Stream Input.nbPlayer}
                      {Delay {RandomThink Input.thinkMin Input.thinkMax}}
                      Left = {MinePlayer Stream  Fired}
                      {SubMarineTurn Stream 0}
                  end %End du if moveplayer
              end %End du ID null
          end %End du ifCount
      end %End de la proc
   in
      case ListPlayers of nil then skip
      []H|T then
         thread {SubMarineTurn H 0} end
         {Simultaneous T}
      end
   end

   % Create  the  port  for  every  player  using  the  PlayerManager  and  assign  a  unique  id
   ListOfPortsOfPlayers = {InitPlayers Input.players Input.colors 1 nil}
   {InitPositions ListOfPortsOfPlayers}
   if Input.isTurnByTurn == true then
      {TurnByTurn ListOfPortsOfPlayers}
   else
      {Simultaneous ListOfPortsOfPlayers}
   end

end
