functor
import
   Input
   Browser
   OS
export
   portPlayer:StartPlayer
define
   StartPlayer
   FindInitialPosition
   Direction
   TreatStream
   FreeSpace
   ReturnColumn
   CheckDamageNumber
   PreviousPos
in
   fun{StartPlayer Color ID}
      Stream
      Port
   in
      {NewPort Stream Port}
      thread
         %{TreatStream Stream MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
         {TreatStream Stream id(id:ID color:Color name:'SuperBasedAI') nil 4 false 0 nil nil 0 nil south}
      end
      Port
   end

   fun{FindInitialPosition Map}
      FindInitialPositionRec in
      fun{FindInitialPositionRec Map I J}
         case Map
         of H|T then
            CheckInRow Pos in
            fun{CheckInRow Row I J}
               case Row
               of Ha|Ta then
                  if Ha == 0 then
                     pt(x:I y:J)
                  else {CheckInRow Ta I J+1}
                  end
               []nil then unit
               end
            end
            Pos = {CheckInRow H I J}
            if I == Input.nRow then
               Pos
            elseif Pos == unit then
               {FindInitialPositionRec T I+1 1}
            else
               Pos
            end
         end
         end
      {FindInitialPositionRec Map 1 1}
   end

  %Retourne la valeur de la case X Y
   fun{FreeSpace PosList X Y}
      fun {IsOOB X Y}
         if X > Input.nRow orelse Y > Input.nColumn orelse X < 1 orelse Y < 1 then true
         else false
         end
      end
      in
      if {IsOOB X Y} then false
      else
         case PosList of H|T then
            if X == 1 then {ReturnColumn H Y}
            else {FreeSpace T X-1 Y}
            end
         []nil then false
         end
      end
   end

   %Cherche la case dans la ligne Actuelle
   fun{ReturnColumn Row Y}
      case Row of nil then false
      []H|T then
         if Y == 1 then
            if H == 0 then true
            else false
            end
         else {ReturnColumn T Y-1}
         end
      end
   end

   fun{PreviousPos Position PosList}
     case PosList of nil then true
     []H|T then
        if H == Position then false
        else {PreviousPos Position T}
        end
     end
   end

   proc{CheckDamageNumber Expos PlayerPos Damage}
      Distance in
      Distance = {Number.abs Expos.x - PlayerPos.x} + {Number.abs Expos.y - PlayerPos.y}
      case Distance of 1 then Damage = 1
      [] 0 then Damage = 2
      else Damage = 0
      end
   end

   proc{TreatStream Stream MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir} % has as many parameters as you want
      case Stream
      of nil then skip
      [] initPosition(?ID ?Position)|T then
         X Y NewPos in
         ID = MyID
         X = {OS.rand} mod Input.nRow + 1
         Y = {OS.rand} mod Input.nColumn + 1
         if {FreeSpace Input.map X Y} == false then {TreatStream Stream MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
         else
            NewPos = pt(x:X y:Y)
            Position = NewPos
            {TreatStream T MyID NewPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
         end
      [] dive|T then
         {TreatStream T MyID MyPos LifeLeft false MineNumber Mines nil SonarCount LastPos LastDir}
      [] isSurface(?ID ?Answer)|T then
         if LifeLeft =< 0 then
           ID = nil
           Answer = Surfaced
         else
           ID = MyID
           Answer = Surfaced
         end
         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
      [] move(?ID ?Position ?Direction)|T then
         Rand NewPos NewDir in
         ID = MyID
         if {List.length PosList} > 3 then
            Position = MyPos
            Direction = surface
         else
             Rand = ({OS.rand} mod 4) + 1
             case Rand of 1 then
                NewPos = pt(x:MyPos.x-1 y:MyPos.y)
                NewDir = north
             [] 2 then
                NewPos = pt(x:MyPos.x+1 y:MyPos.y)
                NewDir = south
             [] 3 then
                NewPos = pt(x:MyPos.x y:MyPos.y-1)
                NewDir = west
             [] 4 then
                NewPos = pt(x:MyPos.x y:MyPos.y+1)
                NewDir = east
             end
             if {FreeSpace Input.map NewPos.x NewPos.y} == true then
                if {PreviousPos NewPos PosList} then
                  Position = NewPos
                  Direction = NewDir
                  {TreatStream T MyID NewPos LifeLeft Surfaced MineNumber Mines NewPos|PosList SonarCount LastPos LastDir}
                else {TreatStream Stream MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
                end
             else
                {TreatStream Stream MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
             end
         end
      [] chargeItem(?ID ?KindItem)|T then
         ID = MyID
         if MineNumber > 4 then
          KindItem = sonar
          {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
         else
          KindItem = missile
          {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber+1 Mines PosList SonarCount LastPos LastDir}
          end
      [] fireItem(?ID ?KindFire)|T then
         ID = MyID
         if LastPos == nil then
            KindFire = sonar
            {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount-1 LastPos LastDir}
         else
            KindFire = missile(pt(x:LastPos.x y:LastPos.y))
            {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber-1 Mines PosList SonarCount nil LastDir}
         end
      [] fireMine(?ID ?Mine)|T then
         ID = MyID
         case Mines of nil then
            Mine = nil
            {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
         []H2|T2 then
            Mine = H2
            {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber T2 PosList SonarCount LastPos LastDir}
         end
      [] sayMove(ID Direction)|T then
         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
      [] saySurface(ID)|T then

         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
      [] sayCharge(ID KindItem)|T then

         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
      [] sayMinePlaced(ID)|T then

         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
      [] sayMissileExplode(ID Position ?Message)|T then
          Damage NextLife in
          {CheckDamageNumber Position MyPos Damage}
          NextLife = LifeLeft - Damage
          if ID == MyID orelse LifeLeft < 1 then Message = nil
          elseif NextLife < 1 then Message = sayDeath(MyID)
          elseif NextLife < LifeLeft then Message = sayDamageTaken(MyID Damage NextLife)
          else Message = nil
          end
         {TreatStream T MyID MyPos NextLife Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
      [] sayMineExplode(ID Position ?Message)|T then
          Damage NextLife in
          {CheckDamageNumber Position MyPos Damage}
          NextLife = LifeLeft - Damage
          if ID == MyID orelse LifeLeft < 1 then Message = nil
          elseif NextLife < 1 then Message = sayDeath(MyID)
          elseif NextLife < LifeLeft then Message = sayDamageTaken(MyID Damage NextLife)
          else Message = nil
          end
         {TreatStream T MyID MyPos NextLife Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
      [] sayPassingDrone(Drone ?ID ?Answer)|T then
         ID = MyID
         case Drone of drone(row X) then if MyPos.x == X then Answer = true else Answer = false end
         []drone(column Y) then if MyPos.y == Y then Answer = true else Answer = false end
         else Answer = false
         end
         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
      [] sayAnswerDrone(Drone ID Answer)|T then
         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
      [] sayPassingSonar(?ID ?Answer)|T then
         Rand in
         Rand = {OS.rand} mod Input.nColumn + 1
         ID = MyID
         Answer = pt(x:MyPos.x y:Rand)
         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
      [] sayAnswerSonar(ID Answer)|T then
         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount Answer LastDir}
      [] sayDeath(ID)|T then

         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
      [] sayDamageTaken(ID Damage LifeLeft)|T then

         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
      [] _|T then
         {TreatStream T MyID MyPos LifeLeft Surfaced MineNumber Mines PosList SonarCount LastPos LastDir}
      end
   end
end
